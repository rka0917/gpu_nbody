#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "cu_util.cuh"


__global__ void test(float3 v){
    v.x = 21.1f;
    printf("I'm fancy because I know it! %.6f\n", v.x);
}

void c_test(){
  float3 v = {0.0f, 0.0f, 0.0f};
  test<<<1,1>>>(v);
  cudaDeviceSynchronize();
  //printf("Position x: %f, y: %f, z:%f", v.x, v.y, v.z);
  std::cout<< v.x << " " << v.y << " " << v.z;
}
