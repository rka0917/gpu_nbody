#include "shape.hpp"

//Method for initializing the
void Shape::init(GLuint shaders, glm::vec4 * pos, int p_size){
    float vertices[9] = {
      -30.0f, -30.0f, 0.0f,
      30.0f, -30.0f, 0.0f,
      0.0f, 30.0f, 0.0f
    };

    // Generate and bind the default VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    //Create and bind the VBO
    glGenBuffers(1, &vbo);


    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //glBindVertexArray(0);

    //Create and bind the SSBO
    //Retrieve the binding index of SSBO
    ssbo_vs = glGetProgramResourceIndex(shaders, GL_SHADER_STORAGE_BLOCK,
                                   "particles_ssbo");

    //Create the SSBO and copy the default content
    glGenBuffers(1, &ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4) * p_size, pos, GL_DYNAMIC_DRAW); //Can i do this?
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    //Register the positon buffer as a shared resource between CUDA and OpenGL
    cudaGraphicsGLRegisterBuffer(&ssbo_gr, ssbo, cudaGraphicsRegisterFlagsNone);
    //Draw wireframes
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    // Set the background color (RGBA)
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //Enable faceculling
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    //Enable depthtest
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);


  }

  void Shape::draw(int p_size){
    //glBindVertexArray(vao);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    //Instanced rendering
    glDrawArraysInstanced(GL_TRIANGLES, 0, 3, p_size);
    //glBindVertexArray(0);
  }

  void Shape::release(){
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ssbo);
    cudaGraphicsUnregisterResource(ssbo_gr);
    printf("OpenGL buffers deallocated.\n");
  }
