#include "common.hpp"
#include "cu_util.cuh"
#include "util.hpp"
#include "shape.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include <chrono>
#include <iostream>
using Clock = std::chrono::steady_clock;
using std::chrono::time_point;
using std::chrono::duration_cast;
using std::chrono::milliseconds;

#define WIDTH 720.0f//1280.0f
#define HEIGHT 640.0f//720.0f
using namespace std;
using namespace glm;
using namespace agp;

//Number of particles used for each collidor
const int iron_particles = 500;
const int silicate_particles = 1500;

GLuint g_default_vao = 0;

int vp_loc;
int fe_loc;
int si_loc;

glm::mat4 view;
glm::mat4 projection;

glm::mat4 VP;

glm::vec4 *p = 0;
size_t size = 0;
int p_size;

glm::vec3 cam_pos = glm::vec3(0.0f,0.0f,12000.0f);
float fov = 90.0f;
int updateVP = 0;

//Class that contains methods for rendering objects
Shape rend;

//Initialize the simulation
/*
Output: initialized simulation with two planets about to collide
*/
void init()
{
    //For measuring time
    time_point<Clock> start = Clock::now();

    p_size = (iron_particles + silicate_particles) * 2;
    printf("iron %d\n", iron_particles);
    printf("silicate %d\n", silicate_particles);

    set_p(iron_particles, silicate_particles);

    //Create the particle buffer and the VP matrix
    view = glm::lookAt(cam_pos,vec3(0.0f,0.0f,0.0f),vec3(0.0f,1.0f,0.0f));
    projection = glm::perspective(glm::radians(fov), WIDTH/HEIGHT,0.1f,300000.0f);
    VP = projection*view;


    //Load the necessary shaders
    GLuint shader = util::loadShaders("vx_shader.glsl","fg_shader.glsl");
    glUseProgram(shader);
    vp_loc = glGetUniformLocation(shader, "VP");
    fe_loc = glGetUniformLocation(shader, "fe_particles");
    si_loc = glGetUniformLocation(shader, "si_particles");

    //Send VP matrix
    glUniformMatrix4fv(vp_loc, 1, GL_FALSE, glm::value_ptr(VP));

    //Initialize the object buffers
    rend.init(shader, p, p_size);

    //Map SSBO to a CUDA buffer
    cudaGraphicsMapResources(1, &rend.ssbo_gr);
    cudaGraphicsResourceGetMappedPointer((void **)&p, &size,
                                        rend.ssbo_gr);
    //Create the particles
    init_cuda(p);

    //Add colour
    glUniform1i(fe_loc, iron_particles);
    glUniform1i(si_loc, silicate_particles);

    //Print out the time for initialization
    time_point<Clock> end = Clock::now();
    milliseconds diff = duration_cast<milliseconds>(end - start);
    std::cout <<"inint time"<< diff.count() << '\n';
}

//Releases buffers used in CUDA part and OpenGL part
void release()
{
    // Release the default VAO and the buffers
    rend.release();
    // Deallocate the buffers used in the CUDA module
    terminate_cuda();
}

//Changes FOV to simulate zooming
/*
Output: updated VP matrix with new FOV
*/
void changeFOV(){
  projection = glm::perspective(glm::radians(fov), WIDTH / HEIGHT,
                                                0.1f, 300000.0f);
  updateVP = 1;
}

//Listener for pressed keyboard keys
/*
Input: key pressed
Output: handle key press
*/
void keybord(unsigned char key, int x, int y){
  switch ( key )
  {
    case 27: // Escape key
      glutLeaveMainLoop();
      break;
    case 43:
      fov -=1;
      changeFOV();
      break;
    case 45:
      fov +=1;
      changeFOV();
      break;
  }
}

//Function for rotating camera
/*
Input: angle
Output: updated VP matrix with new angle
*/
void rotateCamera(float angle){
  cam_pos = glm::rotateY(cam_pos,glm::radians(angle));
  view = glm::lookAt(cam_pos,vec3(0.0f,0.0f,0.0f),vec3(0.0f,1.0f,0.0f));
  updateVP = 1;
}

//Listener for special pressed keyboard keys
/*
Input: special key pressed
Output: handle key press
*/
void spec_keybord(int key, int x, int y){
  switch ( key ){
    case GLUT_KEY_RIGHT:
      rotateCamera(2.5f);
      break;

    case GLUT_KEY_LEFT:
      rotateCamera(-2.5f);
      break;
    default:
      break;
  }
}

//Main render loop
void display()
{
    time_point<Clock> start0 = Clock::now();
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //If any changes have been applied to camera, change VP
    if(updateVP == 1){
      VP = projection*view;
      glUniformMatrix4fv(vp_loc, 1, GL_FALSE, glm::value_ptr(VP));
      updateVP = 0;
    }

    //Unmap the SSBO from CUDA part to be available to OpenGL
    cudaGraphicsUnmapResources(1, &rend.ssbo_gr);
    //Bind SSBO to vertex shader
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, rend.ssbo_vs, rend.ssbo);

    //Draw particles
    rend.draw(p_size);

    //Map SSBO to a CUDA buffer
    cudaGraphicsMapResources(1, &rend.ssbo_gr);
    cudaGraphicsResourceGetMappedPointer((void **)&p, &size,
                                        rend.ssbo_gr);


    //time
    time_point<Clock> start = Clock::now();
    update_cuda(p);
    time_point<Clock> end = Clock::now();
    milliseconds diff = duration_cast<milliseconds>(end - start);
    milliseconds difftot = duration_cast<milliseconds>(end - start0);

    std::cout << "update cuda"<<diff.count() << '\n';
    std::cout << "dispalytime"<<difftot.count() << '\n';

    //Swap the buffers
    glutSwapBuffers();
    glutPostRedisplay();
}


/*
Output: initialize simulation and enter infinite updating and rendering loop
*/
int main(int argc, char **argv)
{
    // Initialize FreeGLUT and create the window
    glutInit(&argc, argv);
    // Setup the window (e.g., size, display mode and so on)
    glutInitWindowSize(WIDTH,HEIGHT);
    glutInitWindowPosition(0,0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);


    // Make FreeGLUT return from the main rendering loop
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                  GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    // Create the window and associate the callbacks
    glutCreateWindow("N-Body simulation");
    glutDisplayFunc(display);
    glutKeyboardFunc(keybord);
    glutSpecialFunc(spec_keybord);

    // Init GLAD to be able to access the OpenGL API
    if (!gladLoadGL())
    {
        return GL_INVALID_OPERATION;
    }

    // Display OpenGL information
    util::displayOpenGLInfo();

    // Initialize the buffer and such
    init();

    // Launch the main loop for rendering
    glutMainLoop();

    // Release all the allocated memory
    release();

	return 0;
}
