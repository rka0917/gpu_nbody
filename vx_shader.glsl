#version 450 core
layout (location = 0) in vec3 aPos;
uniform mat4 VP;
uniform int fe_particles;
uniform int si_particles;
out vec4 out_color;
//SSBO buffer for all particle positions
buffer particles_ssbo
{
    vec4 pos[];
};

void main(){
  // Retrieve the particle position from the SSBO
  vec4 position = pos[gl_InstanceID];

  //Calculate colour for fragment shader
  if((gl_InstanceID < fe_particles) || ((gl_InstanceID < 2*fe_particles + si_particles)&&(gl_InstanceID >fe_particles + si_particles))){
    out_color = glm::vec4(0.9f, 0.1f, 0.9f, 1.0f);
  } else {
    out_color = glm::vec4(0.1f, 0.9f, 0.3f, 1.0f);
  }

  // Calculate the final position of the vertex
  gl_Position = VP * (vec4(aPos, 1) + position);
}
