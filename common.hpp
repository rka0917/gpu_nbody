#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <glad/glad.h>
#include <GL/gl.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <time.h>
#include </usr/local/cuda-9.1/include/cuda_runtime_api.h>
#include </usr/local/cuda-9.1/include/cuda_gl_interop.h>

#define PATH_MAX    4096
#define GL_SUCCESS  0
#define NUM_OF_PARTICLES 200
#define MAX_POS 30.0f

typedef uint8_t BYTE;
