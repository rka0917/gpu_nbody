#version 450 core
out vec4 FragColor;
in vec4 out_color;



void main(){
  //Calculated render colour from vertex shader
  FragColor = out_color;
}
