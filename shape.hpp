#ifndef SHAPE
#define SHAPE
#include "common.hpp"
class Shape {
  GLuint vao;
  GLuint vbo;

public:
  GLuint ssbo_vs;
  GLuint ssbo;
  cudaGraphicsResource_t ssbo_gr;
  void init(GLuint, glm::vec4 *, int);
  void draw(int);
  void release();
};
#endif
