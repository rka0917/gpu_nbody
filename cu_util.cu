#include <ctime>
#include <math.h>
#include <stdio.h>
#include "common.hpp"
#include <curand_kernel.h>
#include "cu_util.cuh"
//TODO Look at the scale down so that it is done properly (or change it back and just adjust the camera)
#define TPB 512
#define PI 3.1416f
#define TOTAL_MASS (5.972f * powf(10.0f, 24.0f)) //Mass of earth
#define PLANET_RADIUS 6371.0f //radius of earth = 6371.0f
#define CORE_RADIUS 3185.5f //radius of inner core of earth = 3185.5f, said previously 1220.0f
#define X_CENTER 3185.0f
#define Z_CENTER 0.0f
#define D 376.78f //Diameter of an element (applies to both iron and silicate)
#define G (6.67408f * powf(10.0f,-20.0f)) //Converted to km3
#define M_Si (7.4161f * powf(10.0f,20.0f)) //Actual mass (7.4161f * powf(10.0f,19.0f))
#define M_Fe (1.9549f * powf(10.0f,21.0f)) //Actual mass (1.9549f * powf(10.0f,20.0f))
#define SPD_si 0.001f
#define SPD_fe 0.002f
#define KRP_si 0.01f
#define KRP_fe 0.02f
#define K_si (2.9114f * powf(10.0f, 8.0f))
#define K_fe (5.8228 * powf(10.0f, 8.0f))
#define TIME_STEP 5.8117f
#define OMEGA ((2 * PI) / (TIME_STEP * 3600)) //Was ((2 * PI) / TIME_STEP) before
#define EPSILON 47.0975f

cudaEvent_t start, stop;


//UNITS USED: km for distance and s for time, kg for weight
int iron_particles;       //iron_particles = (TOTAL_MASS * 0.3f) / M_Fe;
int silicate_particles;  //silicate_particles = (TOTAL_MASS * 0.7f) / M_Si;

Particle *cuda_particles = 0;
Particle *cuda_particles_new = 0;

__device__ float3 create_rho_values(int id){
  curandState state;
  curand_init((unsigned long long)clock() + id, 0, 0, &state);
  float3 rho;
  rho.x = curand_uniform(&state);
  rho.y = curand_uniform(&state);
  rho.z = curand_uniform(&state);

  return rho;
}

//Initiate particle properties, such as initial positions and velocities
/*
Input: Particle buffer, number of iron and silicate particles, position buffer
*/
__global__ void init_particles(Particle *particles, int iron_particles, int silicate_particles, glm::vec4 *positions){
  const int id = blockIdx.x * blockDim.x + threadIdx.x;

  int num_particles = (iron_particles + silicate_particles)*2;
  if(id >= num_particles)
    return;

  float3 rho = create_rho_values(id);

  float my = 1.0f - 2.0f * (rho.y);
  float my_sqrd = powf(my, 2.0);

  //Set initial positions and particle types
  if((id < iron_particles) || ((id < 2*iron_particles + silicate_particles)&&(id>iron_particles + silicate_particles))){

    //Iron particles, part of the inner core
    float rrho = CORE_RADIUS * powf((float)rho.x, 1.0f/3.0f);
    particles[id].type = 1;
    particles[id].pos.x = rrho * powf(1.0 - my_sqrd, 1.0f/2.0f) * cosf(2 * PI * (float)rho.z);
    particles[id].pos.y = rrho * powf(1.0 - my_sqrd, 1.0f/2.0f) * sinf(2 * PI * (float)rho.z);
    particles[id].pos.z = rrho * my;
    if(id>iron_particles + silicate_particles){
      particles[id].pos.x += PLANET_RADIUS + CORE_RADIUS / 2;
    }
    else{
      particles[id].pos.x -= PLANET_RADIUS + CORE_RADIUS / 2;
    }
  }
  else{
    //Silicate particles, part of the outer shell
    float r1 = powf((float) CORE_RADIUS, 3.0f);
    float r2 = powf((float) PLANET_RADIUS, 3.0f);
    float rrho = powf(r1 + (r2 - r1) * rho.x, 1.0f/3.0f);
    particles[id].type = 0;
    particles[id].pos.x = rrho * powf(1.0f - my_sqrd, 1.0f/2.0f) * cosf(2 * PI * (float)rho.z);
    particles[id].pos.y = rrho * powf(1.0f - my_sqrd, 1.0f/2.0f) * sinf(2 * PI * (float)rho.z);
    particles[id].pos.z = rrho * my;
    if(id>iron_particles + silicate_particles){
      particles[id].pos.x += PLANET_RADIUS;
    }
    else{
      particles[id].pos.x -= PLANET_RADIUS;
    }
  }
  //Put position in buffer that is returned
  positions[id].x = particles[id].pos.x;
  positions[id].y = particles[id].pos.y;
  positions[id].z = particles[id].pos.z;
  //Set initial velocities and offset the inital position
  if(id < num_particles / 2){
    particles[id].velocity.x = 3.2416f; //km/s;
    particles[id].velocity.y = 0.0f;
    particles[id].velocity.z = 0.0f;
    //particles[id].pos.x += X_CENTER;
  } else {
    particles[id].velocity.x = -3.2416f; //km/s;
    particles[id].velocity.y = 0.0f;
    particles[id].velocity.z = 0.0f;
    //particles[id].pos.x -= X_CENTER;
    }
}

//Calculate distance between two particles
/*
Input: Particles p1 and p2
Output: Straightline distance between p1 and p2
*/
__device__ float fdist(Particle p1, Particle p2){
    float rx = powf(p1.pos.x - p2.pos.x, 2.0f);
    float ry = powf(p1.pos.y - p2.pos.y, 2.0f);
    float rz = powf(p1.pos.z - p2.pos.z, 2.0f);
    return sqrtf(rx + ry + rz);
}

//Calculate force impact between two silicate particles
/*
Input: Affected particle p1, affector particle p2, distance r, old distance old_r
Output: Total force effect from p2 on p1
*/
__device__ float calc_si_si(Particle p1, Particle p2, float r, float old_r){
  float force;

  if(D <= r){
    force = -G*M_Si*M_Si / powf(r, 2.0f);
  }
  else if(D-D*SPD_si <= r && r < D) {
    force = G*M_Si*M_Si / powf(r, 2.0f) - 0.5 * (K_si + K_si) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(EPSILON <= r && r < D-D*SPD_si) {
    if(old_r > r) // if separation is decreasing
      force = G*M_Si*M_Si / powf(r, 2.0f) - 0.5 * (K_si + K_si) * (powf(D, 2.0f) - powf(r, 2.0f));
    else
      force = G*M_Si*M_Si / powf(r, 2.0f) - 0.5 * (K_si*KRP_si + K_si*KRP_si) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(r < EPSILON){
    force = G*M_Si*M_Si / powf(EPSILON, 2.0f) - 0.5 * (K_si + K_si) * (powf(D, 2.0f) - powf(EPSILON, 2.0f));
  }

  return force;
}
//Calculate force between an iron and silicate particle
/*
Input: Affected particle p1, affector particle p2, distance r, old distance old_r
Output: Total force effect from p2 on p1
*/
__device__ float calc_si_fe(Particle p1, Particle p2, float r, float old_r){
  float force;

  if(D <= r){
    force = -G*M_Si*M_Fe / powf(r, 2.0f);
  }
  else if(D-D*SPD_si <= r && r < D) {
    force = G*M_Si*M_Fe / powf(r, 2.0f) - 0.5 * (K_si + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(D-D*SPD_fe <= r && r < D-D*SPD_si) {
    if(old_r > r) // if separation is decreasing
      force = G*M_Si*M_Fe / powf(r, 2.0f) - 0.5 * (K_si + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
    else
      force = G*M_Si*M_Fe / powf(r, 2.0f) - 0.5 * (K_si*KRP_si + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(EPSILON <= r && r < D-D*SPD_fe) {
    if(old_r > r) // if separation is decreasing
      force = G*M_Si*M_Fe / powf(r, 2.0f) - 0.5 * (K_si + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
    else
      force = G*M_Si*M_Fe / powf(r, 2.0f) - 0.5 * (K_si*KRP_si + K_fe*KRP_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(r < EPSILON){
    force = G*M_Si*M_Fe / powf(EPSILON, 2.0f) - 0.5 * (K_si + K_fe) * (powf(D, 2.0f) - powf(EPSILON, 2.0f));
  }

  return force;
}
//Calculate force between two iron particles
/*
Input: Affected particle p1, affector particle p2, distance r, old distance old_r
Output: Total force effect from p2 on p1
*/
__device__ float calc_fe_fe(Particle p1, Particle p2, float r, float old_r){
  float force;

  if(D <= r){
    force = -G*M_Fe*M_Fe / powf(r, 2.0f);
  }
  else if(D-D*SPD_fe <= r && r < D) {
    force = G*M_Fe*M_Fe / powf(r, 2.0f) - 0.5 * (K_fe + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(EPSILON <= r && r < D-D*SPD_fe) {
    if(old_r > r) // if separation is decreasing
      force = G*M_Fe*M_Fe / powf(r, 2.0f) - 0.5 * (K_fe + K_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
    else
      force = G*M_Fe*M_Fe / powf(r, 2.0f) - 0.5 * (K_fe*KRP_fe + K_fe*KRP_fe) * (powf(D, 2.0f) - powf(r, 2.0f));
  }
  else if(r < EPSILON){
    force = G*M_Fe*M_Fe / powf(EPSILON, 2.0f) - 0.5 * (K_fe + K_fe) * (powf(D, 2.0f) - powf(EPSILON, 2.0f));
  }
  return force;
}
//Calculates the force impact that p2 has on p1
/*
Input: Affected particle p1 and affector particle p2, as well as old versions of affected particle and affector particle
Output: Force as a vector affecting the particle
*/
__device__ float3 calculate_force(Particle p1, Particle p2, Particle oldP1, Particle oldP2){
  //Calculate distnance between p1 and p2
  float r = fdist(p1, p2);
  float old_r = fdist(oldP1, oldP2);
  float force;
  //Get particle types to use corresponding numbers in calc
  switch(p1.type + p2.type * 2){
    case 0:{          //Silicate - silicate
      force = calc_si_si(p1, p2, r, old_r);
      break;
    }
    case 1:{          //iron - silicate
      force = calc_si_fe(p1, p2, r, old_r);
      break;
    }
    case 2:{          //Silicate - iron
      force = calc_si_fe(p1, p2, r, old_r);
      break;
    }
    case 3:{          //iron - iron
      force = calc_fe_fe(p1, p2, r, old_r);
      break;
    }
  }
  // extract x-y-z force components from the total force
  if(r < EPSILON) r = EPSILON;

  float delta_x = (p1.pos.x - p2.pos.x);
  float delta_y = (p1.pos.y - p2.pos.y);
  float delta_z = (p1.pos.z - p2.pos.z);

  return make_float3(force * (delta_x / r), force * (delta_y / r), force * (delta_z / r));
}

//Updating particle forces
/*
Input: Particle buffers containing old and new particle data, number of particles in simulation
*/
__global__ void update_particles_force(Particle *new_particles, Particle *old_particles, int num_particles){
  const int id = blockIdx.x * blockDim.x + threadIdx.x;
  if(id >= num_particles*num_particles)
    return;
    //Calculate new linear velocity for each particle, based on force interactions
    /* Steps for calculating linear velocities:
    1. Compare the particle with every other particle in the system and calculate
    the combined total force impact on the particles in x, y and z dimensions.
    2. When we have the total force impact of a particle, use formula
    ax = Fx / m, ay = Fy / m, az = Fz / m to get acceleration of particle.
    3. Calculate vx =  v0x + ax * dt, vy = v0y + ay * dt, vz = v0z + az * dt
    */

  //Find id for affected and affector particle
  const int my_particle = id % num_particles;
  const int affector = id / num_particles;

  float3 force = calculate_force(new_particles[my_particle], new_particles[affector], old_particles[my_particle], old_particles[affector]);
  float3 v;

  if(new_particles[my_particle].type == 0){
    v.x = (force.x / M_Si)  * TIME_STEP;
    v.y = (force.y / M_Si)  * TIME_STEP;
    v.z = (force.z / M_Si)  * TIME_STEP;
  } else {
    v.x = (force.x / M_Fe)  * TIME_STEP;
    v.y = (force.y / M_Fe)  * TIME_STEP;
    v.z = (force.z / M_Fe)  * TIME_STEP;
  }

  //Atomically add new velocity to the buffer
  atomicAdd(&new_particles[my_particle].velocity.x, v.x);
  atomicAdd(&new_particles[my_particle].velocity.y, v.y);
  atomicAdd(&new_particles[my_particle].velocity.z, v.z);

}

//Take the particles and update their positions and their angular velocities
/*
Input: all particles in this time step and the previous one, the number of particles and a positions list
Output: all particles with updated positions by one time step
*/
__global__ void update_particles_positions(Particle *new_particles, Particle *old_particles, int num_particles, glm::vec4 *positions){
  const int id = blockIdx.x * blockDim.x + threadIdx.x;
  if(id >= num_particles)
    return;

  //First calculate the new position based on the current velocity (put in own function?)
  float delta_x = old_particles[id].velocity.x * TIME_STEP;
  float delta_y = old_particles[id].velocity.y * TIME_STEP;
  float delta_z = old_particles[id].velocity.z * TIME_STEP;

  new_particles[id].pos.x = old_particles[id].pos.x + delta_x;
  new_particles[id].pos.y = old_particles[id].pos.y + delta_y;
  new_particles[id].pos.z = old_particles[id].pos.z + delta_z;
  new_particles[id].type = old_particles[id].type;

  new_particles[id].velocity = old_particles[id].velocity;

  positions[id].x = new_particles[id].pos.x;
  positions[id].y = new_particles[id].pos.y;
  positions[id].z = new_particles[id].pos.z;
}

/*
Input: shared buffer between CUDA part and OpenGL part
Output: particles generated into two planetary bodies with silicate shells and iron cores
*/
void init_cuda(glm::vec4 *cuda_positions){ //ADDED

  //Calculate number of particles per planet * 2 planets
  int num_particles = (iron_particles + silicate_particles) * 2;

  cudaMalloc(&cuda_particles, sizeof(Particle) * num_particles);
  cudaMalloc(&cuda_particles_new, sizeof(Particle) * num_particles);
  init_particles<<<(num_particles + TPB - 1) / TPB, TPB>>> (cuda_particles, iron_particles, silicate_particles, cuda_positions);
  cudaDeviceSynchronize();

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("Error: %s\n", cudaGetErrorString(err));
}

/*
Input: shared buffer between CUDA part and OpenGL part
Output: updated position and velocities of all particles by one time step
*/
void update_cuda(glm::vec4 *cuda_positions){

cudaEventCreate(&start);
cudaEventCreate(&stop);
  int num_particles = (iron_particles + silicate_particles) * 2;
  //Update positions
  cudaEventRecord(start);
  update_particles_positions<<<(num_particles + TPB - 1) / TPB, TPB>>> (cuda_particles_new, cuda_particles, num_particles, cuda_positions);
  cudaDeviceSynchronize();
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  printf("update part pos%f\n", milliseconds);
  //Update velocities

  cudaEventRecord(start);
  update_particles_force<<<(num_particles*num_particles + TPB - 1) / TPB, TPB>>>(cuda_particles_new, cuda_particles, num_particles);
  cudaDeviceSynchronize();

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  printf("update part force%f\n", milliseconds);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("Error: %s\n", cudaGetErrorString(err));

  cudaEventRecord(start);
  cudaMemcpy(cuda_particles, cuda_particles_new, num_particles * sizeof(Particle), cudaMemcpyDeviceToDevice);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  printf("copy%f\n", milliseconds);

}

//Called from the main file when we are finished with simulation, deallocate buffers
/*
Output: free allocated cuda structures
*/
void terminate_cuda(){
  cudaFree(cuda_particles);
  cudaFree(cuda_particles_new);

  printf("CUDA Buffers deallocated.\n");
}

//Called from the main file when we want the number of current particles
/*
Input: number of iron and number of silicate particles
Output: set number of iron and silicate particles to use
*/
void set_p(int fe, int si){
  iron_particles = fe;
  silicate_particles = si;
}
