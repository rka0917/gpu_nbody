#ifndef CU_UTIL
#define CU_UTIL

struct Particle {
  int type; //0 for silicate, 1 for Iron

  glm::vec3 velocity;
  glm::vec3 pos;
};

void init_cuda(glm::vec4 *);
void update_cuda(glm::vec4 *);
void terminate_cuda();

void set_p(int, int);
#endif
