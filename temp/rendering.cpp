

#include "common.hpp"
#include "util.hpp"

using namespace std;
using namespace glm;
using namespace agp;

GLuint g_default_vao = 0;
unsigned int vertexColorLocation = 0;
unsigned int modelLocation = 0;
unsigned int VPLocation = 0;

const int W_HEIGHT = 1080;
const int W_WIDTH = 1920;

glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
glm::mat4 VP;

glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 8.0f);
float cameraFOV = 90.0f;
glm::vec3 *particlePositions;

//colorCounter is for disco mode!
int colorCounter = 0;
void initializePositions();

void init_openGL()
{
    // Generate and bind the default VAO
    glGenVertexArrays(1, &g_default_vao);
    glBindVertexArray(g_default_vao);

    // Set the background color (RGBA)
    //glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
    //Should be black now
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    // Your OpenGL settings, such as alpha, depth and others, should be
    // defined here! For the assignment, we only ask you to enable the
    // alpha channel.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    //Initialize the shaders
    GLuint openGLprog = util::loadShaders("vertexShader.glsl", "fragmentShader.glsl");
    //Get uniform variable location
    vertexColorLocation = glGetUniformLocation(openGLprog, "NewColour");
    modelLocation = glGetUniformLocation(openGLprog, "model");
    VPLocation = glGetUniformLocation(openGLprog, "VP");
    //Bind the program to the current openGL context
    glUseProgram(openGLprog);
    glUniform4f(vertexColorLocation, 0.9f, 0.9f, 0.1f, 0.5f);

    //Allocate room for particles
    particlePositions = (glm::vec3 *) malloc(NUM_OF_PARTICLES * sizeof(glm::vec3));
    initializePositions();
    //Initiate the first MVP
    //model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
    view = glm::lookAt(cameraPos,
                      glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3(0.0f, 1.0f, 0.0f));

    projection = glm::perspective(glm::radians(cameraFOV), (float) W_WIDTH / (float) W_HEIGHT,
                                                0.1f, 100.0f);

    VP = projection*view;
    glUniformMatrix4fv(VPLocation, 1, GL_FALSE, glm::value_ptr(VP));
}

void release()
{
    // Release the default VAO
    glDeleteVertexArrays(1, &g_default_vao);

    // Do not forget to release any memory allocation here!
    free(particlePositions);
}

//5.4
void initializePositions(){
  srand(time(NULL));
  for(int i = 0; i < NUM_OF_PARTICLES; i++){
    particlePositions[i] = glm::vec3(((float)rand()/(float)(RAND_MAX)) * MAX_POS -0.5f,
                                      ((float)rand()/(float)(RAND_MAX)) * MAX_POS -0.5f,
                                    -((float)rand()/(float)(RAND_MAX)) * MAX_POS);/*glm::vec3((float) 0.8f * i
                                    , 0.0f, 0.0f);*/
  }
}

void changeFOV(float fov){
  cameraFOV = cameraFOV + fov;
  projection = glm::perspective(glm::radians(cameraFOV), (float) W_WIDTH / (float) W_HEIGHT,
                                                0.1f, 100.0f);
  VP = projection*view;
}

//Callback function for when a key is pressed
void keyPressed(unsigned char key, int x, int y)
{
  switch(key){
    case 27:
      glutLeaveMainLoop();
      break;
    case 114:  //key r
      glUniform4f(vertexColorLocation, 0.9f, 0.1f, 0.1f, 0.5f);
      break;
    case 98:  //key b
      glUniform4f(vertexColorLocation, 0.1f, 0.1f, 0.9f, 0.5f);
      break;
    case 103: //key g
      glUniform4f(vertexColorLocation, 0.1f, 0.9f, 0.1f, 0.5f);
      break;
    //For zooming...well, kind of
    case 43: //key +
    if(cameraFOV > 10.0f)
      changeFOV(-1.0f);
      break;
    case 45: //key -
    if(cameraFOV < 90.0f)
      changeFOV(1.0f);
    default:
      printf("Key pressed %c\n", key);
  }
}
//Disco mode!!!
void changeColor(){
  if(colorCounter < 1000)
    glUniform4f(vertexColorLocation, 0.9f, 0.1f, 0.1f, 0.5f);
  else if(colorCounter < 2000)
    glUniform4f(vertexColorLocation, 0.1f, 0.1f, 0.9f, 0.5f);
  else
    glUniform4f(vertexColorLocation, 0.1f, 0.9f, 0.1f, 0.5f);
  colorCounter = (colorCounter + 1) % 3000;
}

void rotateCamera(float angle){
  float xPosition = cameraPos.x;
  float zPosition = cameraPos.z;

  cameraPos.x = glm::cos(glm::radians(angle)) * xPosition + glm::sin(glm::radians(angle)) * zPosition;
  cameraPos.z = -glm::sin(glm::radians(angle)) * xPosition + glm::cos(glm::radians(angle)) * zPosition;
  view = glm::lookAt(cameraPos,
                    glm::vec3(0.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f));
  VP = projection*view;
}

void specialKeyPressed(int key, int x, int y){
  switch(key){
    /*Formulas for calculating new x and z values:
     cos 0 * x + sin 0 * z = new X value
    -sin0 * x + cos 0 * z = new Z value*/
    //Change the position and update the view
    case GLUT_KEY_LEFT: {
      rotateCamera(-1.0f);
      printf("Rotating left!\n");
      break;
    }
    case GLUT_KEY_RIGHT:{
      rotateCamera(1.0f);
      printf("Rotating right!\n");
      break;
    }
  }
}

void generateNewPosition(int i){
 particlePositions[i].x = particlePositions[i].x + ((float)rand()/(float)(RAND_MAX) -0.5f);
 particlePositions[i].y = particlePositions[i].y + ((float)rand()/(float)(RAND_MAX) -0.5f);
 particlePositions[i].z = particlePositions[i].z + ((float)rand()/(float)(RAND_MAX) -0.5f);
}

void display()
{
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //printf("FreeGLUT triggered the display() callback!\n");
    // Your rendering code must be here! Do not forget to swap the
    // front and back buffers, and to force a redisplay to keep the
    // render loop running. This functionality is available within
    // FreeGLUT as well, check the assignment for more information.
    //5.4
    for(int i = 0; i < NUM_OF_PARTICLES; i++){
      changeColor();
      generateNewPosition(i);
      model = glm::translate(mat4(), particlePositions[i]);

      glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(model));
      glUniformMatrix4fv(VPLocation, 1, GL_FALSE, glm::value_ptr(VP));
      //Generate a sphere (and its wired equivalent)
      //glutSolidSphere(0.8, 20, 20);
      glutWireSphere(0.8, 20, 20);
    }

    // Important note: The following function flushes the rendering
    // queue, but this is only for single-buffered rendering. You
    // must replace this function following the previous indications.
    //glFlush();
    //For swapping buffers when using double-buffer rendering
     glutSwapBuffers();
     glutPostRedisplay();
}
/*
int main(int argc, char **argv)
{
    // Initialize FreeGLUT and create the window
    glutInit(&argc, argv);

    // Setup the window (e.g., size, display mode and so on)
    glutInitWindowSize(W_WIDTH, W_HEIGHT);
    glutInitWindowPosition(1, 1);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

    // Make FreeGLUT return from the main rendering loop
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                  GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    // Create the window and associate the callbacks
    glutCreateWindow("Applied GPU Programming");
    glutDisplayFunc(display);
    // glutIdleFunc( ... );
    // glutReshapeFunc( ... );
    glutKeyboardFunc(keyPressed);
    glutSpecialFunc(specialKeyPressed);
    // glutMouseFunc( ... );
    // glutMotionFunc( ... );

    // Init GLAD to be able to access the OpenGL API
    if (!gladLoadGL())
    {
        return GL_INVALID_OPERATION;
    }

    // Display OpenGL information
    util::displayOpenGLInfo();

    // Initialize the 3D view
    init();

    // Launch the main loop for rendering
    glutMainLoop();

    // Release all the allocated memory
    release();

  return 0;
}
*/
